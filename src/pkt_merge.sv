timeunit 1ns;
timeprecision 1ns;

//`define DEBUG

module pkt_merge #(
        parameter DATA_W,
        parameter EMPTY_W
)   (  
                    clk_i,
                    rst_i,
                    offset_i,

                    snk_base_data_i,
                    snk_base_valid_i,
                    snk_base_startofpacket_i,
                    snk_base_endofpacket_i,
                    snk_base_empty_i,
                    snk_base_ready_o,                                                           
                                                            
                    snk_to_insert_data_i,
                    snk_to_insert_valid_i,
                    snk_to_insert_startofpacket_i,
                    snk_to_insert_endofpacket_i,
                    snk_to_insert_empty_i,
                    snk_to_insert_ready_o,
                    
                    src_data_o,
                    src_valid_o,
                    src_startofpacket_o,
                    src_endofpacket_o,
                    src_empty_o,
                    src_ready_i 
    );

    localparam DATA_W_VALID    = 64;
    localparam DATA_BITS_PER_SYMBOL = 8;          
    localparam EMPTY_W_VALID   = $clog2(DATA_W/DATA_BITS_PER_SYMBOL);
    `define SHOW_PARAMS $display("%m DATA_W = %0d, DATA_BITS_PER_SYMBOL = %0d, EMPTY_W = %0d", \
                                        DATA_W, DATA_BITS_PER_SYMBOL, EMPTY_W);

    //checking parameters when object is instantiated
    initial begin
        if((DATA_W != DATA_W_VALID) || (EMPTY_W != EMPTY_W_VALID))
        begin
            $error("Parent instantiated module with wrong parameters!");
            $stop;
        end
    end                                        
        
    // Тактовый сигнал
    input   logic               clk_i;

    // Асинхронный сброс
    input   logic               rst_i;

    // Смещение при вставке пакета (в байтах)
    input   logic [15:0]        offset_i;

    // Входной интерфейс главный
    input   logic [DATA_W-1:0]  snk_base_data_i;
    input   logic               snk_base_valid_i;
    input   logic               snk_base_startofpacket_i;
    input   logic               snk_base_endofpacket_i;
    input   logic [EMPTY_W-1:0] snk_base_empty_i;
    output  logic               snk_base_ready_o;

    // Входной интерфейс с данными для вставки
    input   logic [DATA_W-1:0]  snk_to_insert_data_i;
    input   logic               snk_to_insert_valid_i;
    input   logic               snk_to_insert_startofpacket_i;
    input   logic               snk_to_insert_endofpacket_i;
    input   logic [EMPTY_W-1:0] snk_to_insert_empty_i;
    output  logic               snk_to_insert_ready_o;

    // Выходной интерфейс
    output  logic [DATA_W-1:0]  src_data_o;
    output  logic               src_valid_o;
    output  logic               src_startofpacket_o;
    output  logic               src_endofpacket_o;
    output  logic [EMPTY_W-1:0] src_empty_o;
    input   logic               src_ready_i;
    
    
    //settings
    localparam bit [3:0] BYTES_IN_ONE_WORD = 8;
    localparam bit [6:0] BITS_IN_ONE_WORD = BYTES_IN_ONE_WORD * 8;
    localparam BITS_IN_BYTE = 8;
    localparam shortint unsigned MAX_BYTES_IN_PACKET = 65_535;
    localparam shortint unsigned MAX_WORDS_IN_PACKET = MAX_BYTES_IN_PACKET/BYTES_IN_ONE_WORD + 1;
    localparam shortint unsigned MAX_OFFSET_IN_BYTES = 65_535;
    localparam bit [DATA_W-1:0] POSITIVE_WORD_MASK = 64'hFF_FF_FF_FF_FF_FF_FF_FF;    
    
    //******************OFFSET and EMPTY********************
    logic [15:0] offset_mem;

    always @(posedge clk_i, posedge rst_i)
    if(rst_i) offset_mem <= 'h1;
    else
        if(snk_base_valid_i && snk_base_startofpacket_i && snk_base_ready_o) offset_mem <= offset_i; //save new offset when base starts to transmit

    //*******************************************************

    typedef enum logic [2:0] {enIDLE, enSOURCING_BASE_BEFORE_MERGE, enONE_WORD_MERGING, enSTART_MERGING_AT_THIS_WORD, 
                              enSOURCING_INSERT, enFINISH_MERGING, enSOURCING_BASE_AFTER_MERGE, enEND} conveyor_state_t;
    conveyor_state_t conveyor_state, conveyor_prev_state;

    //*****************MEMMORY, POINTERS**********************
    logic [EMPTY_W-1:0] base_empty_bytes_in_last_word, insert_empty_bytes_in_last_word;
    logic [DATA_W-1:0] base_packet_mem [MAX_WORDS_IN_PACKET]    /* synthesis ramstyle = "M10K" */;
    logic [DATA_W-1:0] insert_packet_mem [MAX_WORDS_IN_PACKET]  /* synthesis ramstyle = "M10K" */;
    logic [$clog2(MAX_WORDS_IN_PACKET)-1:0] base_ptr_word_read, base_ptr_word_write, base_max_ptr;
    logic [$clog2(MAX_WORDS_IN_PACKET)-1:0] insert_ptr_word_read, insert_ptr_word_write, insert_max_ptr;
    logic is_it_one_word_base_pkt, is_it_one_word_insert_pkt;
    
    wire have_both_packets_come = ((is_it_one_word_insert_pkt || insert_max_ptr) && (is_it_one_word_base_pkt || base_max_ptr));   //both have come

    wire insert_pkt_is_writing = insert_ptr_word_write > 1; //">1 instead >0" cause if 1 word read and then paused (valid = low) 
    wire base_pkt_is_writing = base_ptr_word_write > 1;     //we don't know if it is 2 word or >2 packet. so conveyor couldn't decide which
                                                            //branch to choose. so conveyor shouldn't start

    wire got_some_data_from_both_packets =  (insert_pkt_is_writing && base_pkt_is_writing) ||                           //both writing
                                            (insert_pkt_is_writing && (base_max_ptr || is_it_one_word_base_pkt)) ||     //insert writing, base has come
                                            ((insert_max_ptr || is_it_one_word_insert_pkt) && base_pkt_is_writing) ||   //insert has come, base writing
                                            have_both_packets_come;

    wire is_conveyor_rdy_to_merge = conveyor_state == enIDLE;
    wire did_conveyor_start_stopping = conveyor_state == enEND;
    wire is_conveyor_stoped = conveyor_prev_state == enEND;


    wire should_conveyor_continue = ((snk_to_insert_valid_i && snk_base_valid_i) ||                              //both writing with valid                      
                                     (snk_to_insert_valid_i && (base_max_ptr || is_it_one_word_base_pkt)) ||     //insert writing with valid. base has come
                                     ((insert_max_ptr || is_it_one_word_insert_pkt) && snk_base_valid_i) ||      //insert has come. base writing with valid
                                     have_both_packets_come ||                                                   //both packets has come. no reasons to stop
                                     did_conveyor_start_stopping || is_conveyor_rdy_to_merge) &&
                                     src_ready_i;                                                                //next device is rdy                                                                            
    


    logic prev_src_valid_o;
    always @(posedge clk_i, posedge rst_i)
        if(rst_i) prev_src_valid_o <= 0;
        else prev_src_valid_o <= src_valid_o;

    wire src_valid_o_goes_down = prev_src_valid_o && ~src_valid_o;


    //write base
    always @(posedge clk_i, posedge rst_i)
        if(rst_i)
        begin
            base_max_ptr <= 0;
            base_empty_bytes_in_last_word <= 0;
            base_ptr_word_write <= 0; 
        end
        else
        begin
            if(snk_base_valid_i && snk_base_ready_o) 
            begin
                base_empty_bytes_in_last_word <= 0;
                base_packet_mem[base_ptr_word_write] <= snk_base_data_i;            
                if(snk_base_endofpacket_i) 
                begin
                    base_empty_bytes_in_last_word <= snk_base_empty_i;
                    base_max_ptr <= base_ptr_word_write;
                end    
                else base_ptr_word_write <= base_ptr_word_write + 'h1;   
            end
            else
            begin
                if(did_conveyor_start_stopping) 
                begin
                    base_max_ptr <= 0; 
                    base_ptr_word_write <= 0;
                end        
            end    
        end    
    
    //write insert
    always @(posedge clk_i, posedge rst_i)
        if(rst_i)
        begin
            insert_max_ptr <= 0;
            insert_empty_bytes_in_last_word <= 0;
            insert_ptr_word_write <= 0;
        end
        else
        begin
            if(snk_to_insert_valid_i && snk_to_insert_ready_o)
            begin
                insert_empty_bytes_in_last_word <= 0;
                insert_packet_mem[insert_ptr_word_write] <= snk_to_insert_data_i;        
                if(snk_to_insert_endofpacket_i)
                begin
                    insert_empty_bytes_in_last_word <= snk_to_insert_empty_i;
                    insert_max_ptr <= insert_ptr_word_write;                
                end    
                else insert_ptr_word_write <= insert_ptr_word_write + 'h1;
            end
            else 
            begin
                if(did_conveyor_start_stopping) 
                begin
                    insert_ptr_word_write <= 0;
                    insert_max_ptr <= 0;
                end    
            end    
        end    


    //one word base package detect
    always @(posedge clk_i, posedge rst_i)
        if(rst_i) is_it_one_word_base_pkt <= 0;
        else
        begin
            if(snk_base_valid_i && snk_base_startofpacket_i && snk_base_endofpacket_i &&
               snk_base_ready_o) is_it_one_word_base_pkt <= 1; //it is one word pkt. max = 0

            else if(did_conveyor_start_stopping) is_it_one_word_base_pkt <= 0;
        end    

    //one word base package detect
    always @(posedge clk_i, posedge rst_i)
        if(rst_i) is_it_one_word_insert_pkt <= 0;
        else
        begin    
            if(snk_to_insert_valid_i && snk_to_insert_startofpacket_i &&
               snk_to_insert_endofpacket_i && snk_to_insert_ready_o) is_it_one_word_insert_pkt <= 1; //it is one word pkt. max = 0

            else if(did_conveyor_start_stopping) is_it_one_word_insert_pkt <= 0;    
        end    

    //when base mem ready
    always @(posedge clk_i, posedge rst_i)
        if(rst_i) snk_base_ready_o <= 1;
        else 
        begin
            if(snk_base_valid_i && snk_base_endofpacket_i && snk_base_ready_o) snk_base_ready_o <= 0; //we have got the base pkt
            else if(src_valid_o_goes_down && is_conveyor_rdy_to_merge) snk_base_ready_o <= 1; //ready to get new pkt when merging is finished
        end

    //when insert mem ready
    always @(posedge clk_i, posedge rst_i)
        if(rst_i) snk_to_insert_ready_o <= 1;
        else
        begin
            if(snk_to_insert_valid_i && snk_to_insert_endofpacket_i && snk_to_insert_ready_o) snk_to_insert_ready_o <= 0;
            else if(src_valid_o_goes_down && is_conveyor_rdy_to_merge) snk_to_insert_ready_o <= 1; //&& cauze valid might go down NOT only when merged finished
        end       
    //*******************************************************
   

    //***********************MERGING CONVEYOR****************
    //condition when packets go sequentially
    logic is_offset_equal_to_base_packet_length;
    
    always @(posedge clk_i)
        is_offset_equal_to_base_packet_length <= is_it_one_word_base_pkt ?
                                                 offset_mem == BYTES_IN_ONE_WORD - base_empty_bytes_in_last_word :
                                                 base_max_ptr ? (offset_mem == ((base_max_ptr + 1) * BYTES_IN_ONE_WORD - base_empty_bytes_in_last_word)) : 0;
    
    //wires which are updated with offset_mem
    wire [$clog2(MAX_WORDS_IN_PACKET)-1:0] insert_would_be_at_this_word = offset_mem/BYTES_IN_ONE_WORD; //word number in base package where insertion would happen to
    logic [$clog2(BYTES_IN_ONE_WORD)-1:0] insert_would_be_at_this_byte_in_word; 
    logic [$clog2(BITS_IN_ONE_WORD):0] bias_in_bits_for_insertion_in_words;
    logic [$clog2(BITS_IN_ONE_WORD):0] bias_in_bits_for_words_after_insertion;

    always @(posedge clk_i)
    begin
        insert_would_be_at_this_byte_in_word <= offset_mem - insert_would_be_at_this_word*BYTES_IN_ONE_WORD; //byte number in a word of base package where insertion would happen to
        bias_in_bits_for_insertion_in_words <= insert_would_be_at_this_byte_in_word*BITS_IN_BYTE; //bit number...
        bias_in_bits_for_words_after_insertion <= BITS_IN_ONE_WORD - insert_would_be_at_this_byte_in_word*BYTES_IN_ONE_WORD; //all inserted words would be biased by it 
    end

    //insert
    logic [$clog2(BYTES_IN_ONE_WORD):0] valid_bytes_in_last_inserted_word; 
    logic [$clog2(BITS_IN_ONE_WORD):0] valid_bits_in_last_inserted_word; 
    logic [$clog2(BITS_IN_ONE_WORD):0] empty_bits_in_last_inserted_word;

    always @(posedge clk_i, posedge rst_i)
        if(rst_i)
        begin
            valid_bytes_in_last_inserted_word <= 0;
            valid_bits_in_last_inserted_word <= 0;
            empty_bits_in_last_inserted_word <= 0;
        end
        else
        begin
            valid_bytes_in_last_inserted_word <= BYTES_IN_ONE_WORD - insert_empty_bytes_in_last_word; // valid bytes in insert pkt. full words ignored
            valid_bits_in_last_inserted_word <= BITS_IN_ONE_WORD - insert_empty_bytes_in_last_word*BITS_IN_BYTE;
            empty_bits_in_last_inserted_word <= insert_empty_bytes_in_last_word*BITS_IN_BYTE;
        end

    //base
    logic [$clog2(BYTES_IN_ONE_WORD):0] valid_bytes_in_last_base_word;

    always @(posedge clk_i, posedge rst_i)
        if(rst_i) valid_bytes_in_last_base_word <= 0;
        else valid_bytes_in_last_base_word <= BYTES_IN_ONE_WORD - base_empty_bytes_in_last_word; //valid bytes in base pkt. full words ignored

    //empty
    wire [$clog2(BYTES_IN_ONE_WORD)+1:0] sum_of_valid_bytes = valid_bytes_in_last_base_word + valid_bytes_in_last_inserted_word;
    
    //condition for full length (base + insert) merged pkt
    wire is_merged_pkt_length_equal_to_sum_of_base_plus_insert = sum_of_valid_bytes > BYTES_IN_ONE_WORD; //length of merged pkt: base + insert! NOT less for 1!


    logic [EMPTY_W-1:0] current_empty;

    //empty calc
    always @(posedge clk_i)
    begin
        if(sum_of_valid_bytes)
        begin
            if(is_merged_pkt_length_equal_to_sum_of_base_plus_insert) current_empty <= BYTES_IN_ONE_WORD - (sum_of_valid_bytes - BYTES_IN_ONE_WORD); //we got additional word in that case
            else current_empty <= BYTES_IN_ONE_WORD - sum_of_valid_bytes; //no additional word: total words = base + insert
        end
        else current_empty <= 0; //both packets have full words = 8 bytes
    end 

    /* Idea of the conveyor:

        memory => buffer_stage0[63:0] => shifting => buffer_stage1[191:0] => bitwise OR => src_data_o[63:0]
            (conveyor state)                   (stage0)                              (stage1)

            [63:0]               [191:128]                  [127:64]                         [63:0]
            [base]  =>              [0]               [base part for this word]        [base part for next word]
                                                                |
                                                                |====> src_data_o 
                                                                |
                                [191:128]                   [127:64]                         [63:0]
            [insert]  => [insert part for next word]  [insert part for this word]              [0]
    */
    logic is_conveyor_stage0_active, is_conveyor_stage1_active;
    logic [DATA_W-1:0] base_buffer_stage0, insert_buffer_stage0;
    logic [3*DATA_W-1:0] base_buffer_stage1, insert_buffer_stage1; //[191:128][127:64][63:0]
    logic [DATA_W-1:0] base_left_from_stage, insert_left_from_stage;

    logic is_mask_needed;
    logic do_we_use_extra_cycle_for_additional_word;
    logic is_base_mem_fully_readed;

    assign base_left_from_stage =  base_buffer_stage1[63:0];
    assign insert_left_from_stage = insert_buffer_stage1[191:128]; 

    always @(posedge clk_i, posedge rst_i)
        if(rst_i) src_empty_o <= 0;
        else   
            if(is_conveyor_stage0_active) src_empty_o <= current_empty; //updates when conveyor stage0 is active. 
                                                                        //means that shifting still in progress and we dont already know real empty.
                                                                        //if stage0 is 0 so we know REAL empty and holding it   

    always @(posedge clk_i) 
        if(should_conveyor_continue) conveyor_prev_state <= conveyor_state; //rst isn't needed

    //stage0
    always @(posedge clk_i, posedge rst_i)
        if(rst_i)
        begin
            base_buffer_stage1 <= 0;
            insert_buffer_stage1 <= 0;
            is_conveyor_stage1_active <= 0;
        end
        else
        begin
            if(should_conveyor_continue)
            begin
                is_conveyor_stage1_active <= is_conveyor_stage0_active;
                if(is_conveyor_stage0_active)
                begin            
                    unique case(conveyor_prev_state)
                    
                        //so many comb logic here. Need optimization... 
                        enONE_WORD_MERGING:
                        begin
                            logic [DATA_W-1:0] shift_buffer;
                            logic [2*DATA_W-1:0] buffer_for_2nd_part_of_base_word;
                            `ifdef DEBUG
                                ShowRegsInfo("one word merging");
                            `endif
                            buffer_for_2nd_part_of_base_word = (base_buffer_stage0 >> bias_in_bits_for_insertion_in_words) << (bias_in_bits_for_insertion_in_words + valid_bits_in_last_inserted_word);                        
                            shift_buffer = (base_buffer_stage0 << bias_in_bits_for_words_after_insertion) >> bias_in_bits_for_words_after_insertion; 
                            base_buffer_stage1 <= {64'b0, shift_buffer | buffer_for_2nd_part_of_base_word[63:0], buffer_for_2nd_part_of_base_word[127:64]}; //(b | 0 | b)
                            insert_buffer_stage1 <= {64'b0, MaskLastInsertedWord(insert_buffer_stage0), 64'b0} << bias_in_bits_for_insertion_in_words;                   
                        end


                        //just putting word as it is
                        enSOURCING_BASE_BEFORE_MERGE:
                        begin
                            `ifdef DEBUG
                                ShowRegsInfo("sourcing base before");
                            `endif
                            base_buffer_stage1 <= {64'b0, base_buffer_stage0, 64'b0};
                            insert_buffer_stage1 <= {64'b0, 64'b0, 64'b0};
                        end


                        //shifting base as barrel -> shifting base to righ, insert to left -> merge
                        enSTART_MERGING_AT_THIS_WORD:
                        begin
                            logic [DATA_W-1:0] circular_shift;
                            `ifdef DEBUG
                                ShowRegsInfo("start merge");
                            `endif                        
                            circular_shift = {base_buffer_stage0, base_buffer_stage0} >> bias_in_bits_for_insertion_in_words;                        
                            base_buffer_stage1 <= {64'b0, circular_shift, 64'b0} >> bias_in_bits_for_words_after_insertion;
                            insert_buffer_stage1 <= {64'b0, insert_buffer_stage0, 64'b0} << bias_in_bits_for_insertion_in_words;                                                                   
                        end


                        enSOURCING_INSERT:
                        begin
                            `ifdef DEBUG
                                ShowRegsInfo("sourcing insert");
                            `endif    
                            base_buffer_stage1[127:64] <= base_buffer_stage0;

                            // ( datal mem | datal left << | 0 ) >>  ======>  ( datal left new | data mem + datal left | 0) 
                            if(is_mask_needed)                    
                                insert_buffer_stage1 <= {MaskLastInsertedWord(insert_buffer_stage0), //masking when insert pkt goes after base. so it is last inserted word
                                                        insert_left_from_stage << bias_in_bits_for_words_after_insertion,
                                                        64'b0} >> bias_in_bits_for_words_after_insertion;                                                                                   
                            else 
                                insert_buffer_stage1 <= {insert_buffer_stage0,
                                                        insert_left_from_stage << bias_in_bits_for_words_after_insertion,
                                                        64'b0} >> bias_in_bits_for_words_after_insertion;                                                                      
                        end


                        enFINISH_MERGING:
                        begin
                            logic [2*DATA_W-1:0] shifting_buffer;
                            `ifdef DEBUG
                                ShowRegsInfo("finish merge");
                            `endif    
                            //mask is always needed
                            insert_buffer_stage1 <= {MaskLastInsertedWord(insert_buffer_stage0), //masking empty bytes at last insert word
                                                    insert_left_from_stage << bias_in_bits_for_words_after_insertion, 
                                                    64'b0} >> bias_in_bits_for_words_after_insertion;
                                                
                            //shifting buffer: (0 , datab left, datab mem) << =====> (datab left new, datab mem)
                            //base buffer stage1: (0 , datab mem, datab left new)
                            shifting_buffer = (base_left_from_stage | base_buffer_stage0) << valid_bits_in_last_inserted_word; //might happen that base left is totally empty (offset/8 = int). so we use stage0 in this case
                            base_buffer_stage1 <= {64'b0, shifting_buffer[63:0], shifting_buffer[127:64]};                 
                        end
                        

                        enSOURCING_BASE_AFTER_MERGE:
                        begin
                            logic [2*DATA_W-1:0] shifting_buffer;
                            `ifdef DEBUG
                                ShowRegsInfo("sourcing base after");
                            `endif    
                            //no mask cause we send empty data for last word
                            //shifting buffer: (datab mem , 0) ====> (datab left new , datab mem)
                            //base buffer stage1: ( 0 , datab mem | datab left | datal left , datab left new)
                            //bitwise or between base and insert left cause insert might be NOT empty
                            if(is_it_one_word_insert_pkt) shifting_buffer = {base_buffer_stage0 , 64'b0} >> empty_bits_in_last_inserted_word;                        
                            else shifting_buffer = {base_buffer_stage0 , 64'b0} >> empty_bits_in_last_inserted_word;                    
                            base_buffer_stage1 <= {64'b0, (shifting_buffer[63:0] | base_left_from_stage | insert_left_from_stage), shifting_buffer[127:64]};
                            
                            insert_buffer_stage1 <= {64'b0, 64'b0, 64'b0};
                        end                
                    endcase
                end
                else
                begin
                    base_buffer_stage1 <= 0;
                    insert_buffer_stage1 <= 0;
                end
            end          
        end

    logic was_already_startofpacket_strobe;

    //stage1
    always @(posedge clk_i, posedge rst_i)
        if(rst_i)
        begin
            src_data_o <= 0;
            src_valid_o <= 0;
            src_startofpacket_o <= 0;
            src_endofpacket_o <= 0;
        end
        else
        begin
            if(should_conveyor_continue)
            begin
                if(is_conveyor_stage1_active)
                begin
                    src_data_o <= base_buffer_stage1[127:64] | insert_buffer_stage1[127:64];
                    src_valid_o <= 1;
                    if(~src_valid_o && ~was_already_startofpacket_strobe) //setting startofpacket for first time
                    begin
                        was_already_startofpacket_strobe <= 1;
                        src_startofpacket_o <= 1;
                    end    
                    else src_startofpacket_o <= 0;
                    if(is_conveyor_stoped) src_endofpacket_o <= 1;
                end
                else
                begin
                    src_valid_o <= 0;
                    src_startofpacket_o <= 0;
                    src_endofpacket_o <= 0;
                    was_already_startofpacket_strobe <= 0;
                end
            end
            else 
                if(src_ready_i) src_valid_o <= 0; //no need to put valid low if just rdy is low                 
        end
    //******************************************************


    //****************CONVEYOR STATES CONTROL***************

    always @(posedge clk_i, posedge rst_i)
        if(rst_i)
        begin
            conveyor_state <= enIDLE;
            base_buffer_stage0 <= 0;
            insert_buffer_stage0 <= 0;
            is_base_mem_fully_readed <= 0;
            is_mask_needed <= 0;
            base_ptr_word_read <= 0;
            insert_ptr_word_read <= 0;              
            is_conveyor_stage0_active <= 0;
        end
        else
        begin
            if(should_conveyor_continue) //if next device isn't rdy or some packets have no data valid conveyor should be paused
            begin
                unique case(conveyor_state)

                    //waiting for both pakets and offset_mem
                    enIDLE:
                    begin
                        if(got_some_data_from_both_packets) //ready to start some fun
                        begin
                            if(!insert_would_be_at_this_word) //merging at first word. merge now, there is no pure base sourcing
                                if(is_it_one_word_insert_pkt) conveyor_state <= enONE_WORD_MERGING;  //special case! insert = 1 word and merges at first base word
                                else conveyor_state <= enSTART_MERGING_AT_THIS_WORD; //merged insert word > 1 and takes more than one word
                            else conveyor_state <= enSOURCING_BASE_BEFORE_MERGE; //base packet goes first
                        end
                    end
            
                    //sourcing base packet
                    enSOURCING_BASE_BEFORE_MERGE: 
                    begin
                        is_conveyor_stage0_active <= 1; //turn on the conveyer. might be first step as well
                        if(IsItLastReadingBaseWord()) //it might be one word pkt and offset/8 = int
                        begin
                            ReadBaseMem();
                            is_base_mem_fully_readed <= 1;
                        end
                        else ReadBaseMemAndIncPtr();       
                        if(base_ptr_word_read == insert_would_be_at_this_word - 1) //next word is for merge
                        begin
                            if(insert_would_be_at_this_byte_in_word) //offset/8 != int. Merge required
                            begin
                                if(is_it_one_word_insert_pkt) conveyor_state <= enONE_WORD_MERGING; //special case! insert = 1 word
                                else conveyor_state <= enSTART_MERGING_AT_THIS_WORD; //insert word > 1
                            end    
                            else
                            begin //merging on the edge (offset/8 = int). no start merge is required 
                                if(is_it_one_word_insert_pkt) conveyor_state <= enFINISH_MERGING; //insert = 1 word. Mask it and go out
                                else conveyor_state <= enSOURCING_INSERT; //insert > 1. Sourcing insert   
                            end
                        end                                                
                    end    

                    //special case when insert word is 1 word and is placed inside single base word (base_high, insert, base_low) or splitting for two words 1:(insert low, base low) 2:(base_high, insert_high)
                    enONE_WORD_MERGING:
                    begin
                        is_conveyor_stage0_active <= 1; //turn on the conveyer. it might be first step
                        ReadInsertMem(); //it is one word insert pkt
                        if(IsItLastReadingBaseWord()) //base = last, insert = 1 word
                        begin
                            ReadBaseMem();
                            if(is_merged_pkt_length_equal_to_sum_of_base_plus_insert) //last base + 1 insert = 2 words 
                            begin
                                is_base_mem_fully_readed <= 1;
                                conveyor_state <= enSOURCING_BASE_AFTER_MERGE; 
                            end    
                            else conveyor_state <= enEND; //last base + 1 insert = 1 word
                        end
                        else 
                        begin //sourcing last of base packet
                            ReadBaseMemAndIncPtr(); 
                            conveyor_state <= enSOURCING_BASE_AFTER_MERGE; 
                        end         
                    end

                    //merge base packet and head of insert. insert > 1 word
                    enSTART_MERGING_AT_THIS_WORD: 
                    begin
                        is_conveyor_stage0_active <= 1; //turn on the conveyer if we skiped other steps                
                        ReadInsertMemAndIncPtr();
                        if(IsItLastReadingBaseWord()) 
                        begin
                            ReadBaseMem(); //we need at least 1 base word anyway. Two ways: base words = 1 or insertion would be at last base word 
                            is_base_mem_fully_readed <= 1;
                        end
                        else ReadBaseMemAndIncPtr();
                        if(IsItSecondLastReadingInsertWord()) //insert words = 2  
                        begin
                            if(is_offset_equal_to_base_packet_length) conveyor_state <= enSOURCING_INSERT; //2 insert words and insert goes after base
                            else conveyor_state <= enFINISH_MERGING; //2 insert words but insert is inside base
                        end
                        else conveyor_state <= enSOURCING_INSERT;  //insert words > 2. insert goes now                           
                    end    
                    
                    //inserting additional packet into base packet
                    enSOURCING_INSERT: 
                    begin
                        if(do_we_use_extra_cycle_for_additional_word) //just sourcing additional word
                        begin
                            do_we_use_extra_cycle_for_additional_word <= 0;
                            conveyor_state <= enEND; 
                        end    
                        else
                        begin
                            ClearBaseMem(); //base on stage0 should be zero in this case           
                            if(is_offset_equal_to_base_packet_length) //insert packet goes after base
                            begin
                                if(IsItLastReadingInsertWord()) //reading till last insert word
                                begin                        
                                    ReadInsertMem();
                                    is_mask_needed <= 1;
                                    if(is_merged_pkt_length_equal_to_sum_of_base_plus_insert && insert_would_be_at_this_byte_in_word) do_we_use_extra_cycle_for_additional_word <= 1; //there is data in insert buffer and it goes after base. need extra cycle
                                                                                                                                                                                    //insert at this byte > 0. it means that insertion is inside packet!
                                                                                                                                                                                    //And smth there is in left buffer -> extra cycle to flush out
                                    else conveyor_state <= enEND;
                                end
                                else ReadInsertMemAndIncPtr();
                            end
                            else //insert packet goes inside base
                            begin
                                ReadInsertMemAndIncPtr();
                                if(IsItSecondLastReadingInsertWord()) conveyor_state <= enFINISH_MERGING;                     
                            end  
                        end                                   
                    end

                    //merge tail of insert package
                    enFINISH_MERGING:
                    begin
                        ReadInsertMem(); //it is last reading insert word
                        if(insert_would_be_at_this_byte_in_word || is_base_mem_fully_readed) //if offset/8 IS NOT INT(!!!) do NOT need base word here cause base left + insert left + new insert word > full word 
                        begin                                                                //insert_would_be_at_this_byte_in_word > 0 means that offset/8 != int
                            ClearBaseMem();  
                            if((!is_merged_pkt_length_equal_to_sum_of_base_plus_insert || is_it_one_word_insert_pkt) &&
                                is_base_mem_fully_readed) conveyor_state <= enEND; //pkt is shorter than sum of base + insert OR it is one word insert pkt (it goes last). And base is already readed. so flush all here and end
                            else  conveyor_state <= enSOURCING_BASE_AFTER_MERGE; //sourcing left stuff
                        end
                        else 
                        begin
                            if(IsItLastReadingBaseWord())
                            begin
                                ReadBaseMem();
                                is_base_mem_fully_readed <= 1;
                                if(!is_merged_pkt_length_equal_to_sum_of_base_plus_insert) conveyor_state <= enEND;
                                else conveyor_state <= enSOURCING_BASE_AFTER_MERGE;
                            end
                            else
                            begin 
                                ReadBaseMemAndIncPtr();
                                conveyor_state <= enSOURCING_BASE_AFTER_MERGE;                 
                            end    
                        end
                    end 

                    //sourcing left base packet
                    enSOURCING_BASE_AFTER_MERGE:
                    begin
                        if(do_we_use_extra_cycle_for_additional_word) //just sourcing additional word
                        begin
                            do_we_use_extra_cycle_for_additional_word <= 0;
                            conveyor_state <= enEND;    
                        end
                        else
                        begin
                            ClearInsertMem(); //insert mem is fully readed already 
                            if(IsItLastReadingBaseWord())
                            begin
                                if(is_base_mem_fully_readed) ClearBaseMem(); //base mem is already fully readed in start merging case 
                                                                            //so it is 1 base word or insertion happens at last base word                              
                                else ReadBaseMem();

                                if(is_merged_pkt_length_equal_to_sum_of_base_plus_insert && ~is_base_mem_fully_readed) do_we_use_extra_cycle_for_additional_word <= 1; //there is data in base left buffer. need extra cycle. 
                                else conveyor_state <= enEND;    
                            end
                            else ReadBaseMemAndIncPtr();     
                        end   
                    end

                    //wait till data get out and turn off conveyor
                    enEND:
                    begin
                        is_base_mem_fully_readed <= 0;
                        is_mask_needed <= 0;
                        base_ptr_word_read <= 0;
                        insert_ptr_word_read <= 0;              
                        is_conveyor_stage0_active <= 0;
                        conveyor_state <= enIDLE;
                    end

                endcase
            end                  
        end  
    //*****************************************************


    function logic IsItLastReadingInsertWord();
        if(insert_max_ptr) return(insert_ptr_word_read == insert_max_ptr);
        else 
            if(is_it_one_word_insert_pkt) return 1;
            else return 0;
    endfunction


    function logic IsItSecondLastReadingInsertWord();
        if(insert_max_ptr) return(insert_ptr_word_read == (insert_max_ptr - 1));
        else return(insert_ptr_word_read == (insert_ptr_word_write - 1)); //might happen that valid goes low and 2 words left so max ptr isn't updated
    endfunction


    function logic IsItLastReadingBaseWord();
        if(base_max_ptr) return(base_ptr_word_read == base_max_ptr);
        else 
            if(is_it_one_word_base_pkt) return 1;
            else return 0;
    endfunction


    task ReadBaseMemAndIncPtr();
        base_buffer_stage0 <= base_packet_mem[base_ptr_word_read];
        base_ptr_word_read <= base_ptr_word_read + 'h1;        
    endtask


    task ReadBaseMem();
        base_buffer_stage0 <= base_packet_mem[base_ptr_word_read];
    endtask


    task ReadInsertMemAndIncPtr();
        insert_buffer_stage0 <= insert_packet_mem[insert_ptr_word_read];
        insert_ptr_word_read <= insert_ptr_word_read + 'h1;
    endtask


    task ReadInsertMem();
        insert_buffer_stage0 <= insert_packet_mem[insert_ptr_word_read];
    endtask

    
    task ClearBaseMem();
        base_buffer_stage0 <= 0;
    endtask


    task ClearInsertMem();
        insert_buffer_stage0 <= 0;
    endtask


    function automatic logic [DATA_W-1:0] MaskLastInsertedWord(logic [DATA_W-1:0] last_inserted_word);
        MaskLastInsertedWord = last_inserted_word & (POSITIVE_WORD_MASK >> empty_bits_in_last_inserted_word);
    endfunction


//*************************RESETS*******************************
    initial
    begin
        offset_mem = 1;

        conveyor_state = enIDLE;

        insert_buffer_stage0 = 0;
        base_buffer_stage0 = 0;
        insert_buffer_stage1 = 0;
        base_buffer_stage1 = 0;

        is_conveyor_stage0_active = 0;
        is_conveyor_stage1_active = 0;

        base_ptr_word_read = 0;
        insert_ptr_word_read = 0;
        base_ptr_word_write = 0;
        insert_ptr_word_write = 0;
        base_max_ptr = 0;
        insert_max_ptr = 0;
        
        snk_base_ready_o = 1;
        snk_to_insert_ready_o = 1;

        src_data_o = 0;
        src_valid_o = 0;
        src_startofpacket_o = 0;
        src_endofpacket_o = 0;
        src_empty_o = 0;

        is_mask_needed = 0;
        do_we_use_extra_cycle_for_additional_word = 0;    
        is_it_one_word_base_pkt = 0;
        is_it_one_word_insert_pkt = 0;  

        is_base_mem_fully_readed = 0; 

        prev_src_valid_o = 0;   

        was_already_startofpacket_strobe = 0;  

        valid_bytes_in_last_inserted_word = 0; 
        valid_bits_in_last_inserted_word = 0; 
        empty_bits_in_last_inserted_word = 0;

        valid_bytes_in_last_base_word = 0;
    end
//**************************************************************

//***************************DEBUG STUFF*************************


`ifdef DEBUG                     
    
 /*synthesis translate_off*/   
    initial `SHOW_PARAMS

    initial begin
        @(posedge clk_i);
        #1;
        $display("MAX_WORDS_IN_PACKET = %d", MAX_WORDS_IN_PACKET);
        $display("BITS_IN_ONE_WORD = %d", BITS_IN_ONE_WORD);
        $display("BYTES_IN_ONE_WORD=%d", BYTES_IN_ONE_WORD);
        $display("insert_would_be_at_this_word = %d", insert_would_be_at_this_word);
        $display("insert_would_be_at_this_byte_in_word = %d", insert_would_be_at_this_byte_in_word);
        $display("bias_in_bits_for_words_after_insertion = %d", bias_in_bits_for_words_after_insertion);
    end

    always @*
        if(conveyor_prev_state != conveyor_state) ShowStateInfo();
    

    function void ShowStateInfo();
        $display("%m %t state:  %s -> %s", $time, conveyor_prev_state, conveyor_state);
        $display("%m %t base_ptr_word_write     = %d\t insert_ptr_word_write     = %d", $time, base_ptr_word_write, insert_ptr_word_write);
        $display("%m %t base_max_ptr            = %d\t insert_max_ptr            = %d", $time, base_max_ptr, insert_max_ptr);
        $display("%m %t base_ptr_word_read      = %d\t insert_ptr_word_read      = %d", $time, base_ptr_word_read, insert_ptr_word_read);
        $display("%m %t is_it_one_word_base_pkt = %b\t is_it_one_word_insert_pkt = %b", $time, is_it_one_word_base_pkt, is_it_one_word_insert_pkt);
    endfunction
/*synthesis translate_on*/

    function void ShowRegsInfo(string state_name);
        $display("-----%s-----", state_name);
        $display("time = %t", $time);
        $display("base_buffer_stage0     = %h", base_buffer_stage0);
        $display("base_left_from_stage   = %h", base_left_from_stage);
        $display("insert_buffer_stage0   = %h", insert_buffer_stage0);
        $display("insert_left_from_stage = %h", insert_left_from_stage);
        $display("is_merged_pkt_length_equal_to_sum_of_base_plus_insert = %b", is_merged_pkt_length_equal_to_sum_of_base_plus_insert);
        $display("is_offset_equal_to_base_packet_length = %b", is_offset_equal_to_base_packet_length);      
        $display("insert_would_be_at_this_word = %d", insert_would_be_at_this_word);
        $display("base_ptr_word_read = %d", base_ptr_word_read);
        $display("insert_ptr_word_read = %d", insert_ptr_word_read);
        $display("is_mask_needed = %b", is_mask_needed);
        $display("valid_bits_in_last_inserted_word = %d", valid_bits_in_last_inserted_word);
        $display("bias_in_bits_for_insertion_in_words = %d", bias_in_bits_for_insertion_in_words);
        $display("is_base_mem_fully_readed = %b", is_base_mem_fully_readed);
        $strobe("------------------");
        $strobe("base_buffer_stage1      = %h", base_buffer_stage1[127:64]);
        $strobe("insert_buffer_stage1    = %h", insert_buffer_stage1[127:64]);
        $strobe("base_left_from_stage    = %h", base_left_from_stage);
        $strobe("insert_left_from_stage  = %h", insert_left_from_stage);
        $strobe("------------------");
    endfunction

`endif


//**************************************************************
endmodule

   