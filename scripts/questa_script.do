#create lib
vlib work

#compile
vlog ../src/pkt_merge.sv
vlog ../tb/CRV/tb_top.sv
vlog ../tb/CRV/test.sv

#elaborate without optimization
vsim -novopt work.tb_top 

#waves
add wave -position end  sim:/tb_top/clk
add wave -position end  sim:/tb_top/reset
add wave -position end  sim:/tb_top/b_if/offset_i
add wave -position end  sim:/tb_top/b_if/snk_base_valid_i
add wave -position end  sim:/tb_top/b_if/snk_base_startofpacket_i
add wave -position end  sim:/tb_top/b_if/snk_base_endofpacket_i
add wave -position end  sim:/tb_top/b_if/snk_base_ready_o
add wave -position end  sim:/tb_top/b_if/snk_base_empty_i
add wave -position end  sim:/tb_top/b_if/snk_base_data_i
add wave -position end  sim:/tb_top/i_if/snk_to_insert_valid_i
add wave -position end  sim:/tb_top/i_if/snk_to_insert_startofpacket_i
add wave -position end  sim:/tb_top/i_if/snk_to_insert_endofpacket_i
add wave -position end  sim:/tb_top/i_if/snk_to_insert_ready_o
add wave -position end  sim:/tb_top/i_if/snk_to_insert_empty_i
add wave -position end  sim:/tb_top/i_if/snk_to_insert_data_i
add wave -position end  sim:/tb_top/m_if/src_valid_o
add wave -position end  sim:/tb_top/m_if/src_startofpacket_o
add wave -position end  sim:/tb_top/m_if/src_endofpacket_o
add wave -position end  sim:/tb_top/m_if/src_ready_i
add wave -position end  sim:/tb_top/m_if/src_empty_o
add wave -position end  sim:/tb_top/m_if/src_data_o

#simulate
run -all