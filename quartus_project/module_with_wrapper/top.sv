module top(	clk_i,
            rst_i,
            offset_i_v,
            
            snk_base_data_i_v,
            snk_base_valid_i_v,
            snk_base_startofpacket_i_v,
            snk_base_endofpacket_i_v,
            snk_base_empty_i_v,
            snk_base_ready_o_v,
            
            snk_to_insert_data_i_v,
            snk_to_insert_valid_i_v,
            snk_to_insert_startofpacket_i_v,
            snk_to_insert_endofpacket_i_v,
            snk_to_insert_empty_i_v,
            snk_to_insert_ready_o_v,
            
            src_data_o_v,
            src_valid_o_v,
            src_startofpacket_o_v,
            src_endofpacket_o_v,
            src_empty_o_v,
            src_ready_i_v);


    localparam DATA_W    = 64;
    localparam DATA_BITS_PER_SYMBOL = 8;          
    localparam EMPTY_W   = $clog2(DATA_W/DATA_BITS_PER_SYMBOL);

        
    // Тактовый сигнал
    input logic               clk_i;

    // Асинхронный сброс
    input logic               rst_i;

    // Смещение при вставке пакета (в байтах)
    input   logic [15:0]        offset_i_v;

    // Входной интерфейс главный
    input   logic [DATA_W-1:0]  snk_base_data_i_v;
    input   logic               snk_base_valid_i_v;
    input   logic               snk_base_startofpacket_i_v;
    input   logic               snk_base_endofpacket_i_v;
    input   logic [EMPTY_W-1:0] snk_base_empty_i_v;
    output  logic               snk_base_ready_o_v;

    // Входной интерфейс с данными для вставки
    input   logic [DATA_W-1:0]  snk_to_insert_data_i_v;
    input   logic               snk_to_insert_valid_i_v;
    input   logic               snk_to_insert_startofpacket_i_v;
    input   logic               snk_to_insert_endofpacket_i_v;
    input   logic [EMPTY_W-1:0] snk_to_insert_empty_i_v;
    output  logic               snk_to_insert_ready_o_v;

    // Выходной интерфейс
    output  logic [DATA_W-1:0]  src_data_o_v;
    output  logic               src_valid_o_v;
    output  logic               src_startofpacket_o_v;
    output  logic               src_endofpacket_o_v;
    output  logic [EMPTY_W-1:0] src_empty_o_v;
    input   logic               src_ready_i_v;



    // Смещение при вставке пакета (в байтах)
    logic [15:0]        offset_i;

    // Входной интерфейс главный
    logic [DATA_W-1:0]  snk_base_data_i;
    logic               snk_base_valid_i;
    logic               snk_base_startofpacket_i;
    logic               snk_base_endofpacket_i;
    logic [EMPTY_W-1:0] snk_base_empty_i;
    logic               snk_base_ready_o;

    // Входной интерфейс с данными для вставки
    logic [DATA_W-1:0]  snk_to_insert_data_i;
    logic               snk_to_insert_valid_i;
    logic               snk_to_insert_startofpacket_i;
    logic               snk_to_insert_endofpacket_i;
    logic [EMPTY_W-1:0] snk_to_insert_empty_i;
    logic               snk_to_insert_ready_o;

    // Выходной интерфейс
    logic [DATA_W-1:0]  src_data_o;
    logic               src_valid_o;
    logic               src_startofpacket_o;
    logic               src_endofpacket_o;
    logic [EMPTY_W-1:0] src_empty_o;
    logic               src_ready_i;

	pkt_merge #(.DATA_W(64), .EMPTY_W(3)) merger(.*);
						  
						  
	always @(posedge clk_i)
    begin
        offset_i <= offset_i_v;
        snk_base_data_i <= snk_base_data_i_v;
        snk_base_valid_i <= snk_base_valid_i_v;
        snk_base_startofpacket_i <= snk_base_startofpacket_i_v;
        snk_base_endofpacket_i <= snk_base_endofpacket_i_v;
        snk_base_empty_i <= snk_base_empty_i_v;
        snk_base_ready_o_v <= snk_base_ready_o;

        snk_to_insert_data_i <= snk_to_insert_data_i_v;
        snk_to_insert_valid_i <= snk_to_insert_valid_i_v;
        snk_to_insert_startofpacket_i <= snk_to_insert_startofpacket_i_v;
        snk_to_insert_endofpacket_i <= snk_to_insert_endofpacket_i_v;
        snk_to_insert_empty_i <= snk_to_insert_empty_i_v;
        snk_to_insert_ready_o_v <= snk_to_insert_ready_o;

        src_data_o_v <= src_data_o;
        src_valid_o_v <= src_valid_o;
        src_startofpacket_o_v <= src_startofpacket_o;
        src_endofpacket_o_v <=src_endofpacket_o;
        src_empty_o_v <= src_empty_o;
        src_ready_i <= src_ready_i_v;
    end					  
												  

endmodule                    







    