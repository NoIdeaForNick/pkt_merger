timeunit 1ns;
timeprecision 1ns;

module tb_shift_merge;

    localparam DATA_W    = 64;
    localparam DATA_BITS_PER_SYMBOL = 8;          
    localparam EMPTY_W   = $clog2(DATA_W/DATA_BITS_PER_SYMBOL);
    
    logic               clk_i;
    logic               rst_i;
    logic [15:0]        offset_i;
    logic [DATA_W-1:0]  snk_base_data_i;
    logic               snk_base_valid_i;
    logic               snk_base_startofpacket_i;
    logic               snk_base_endofpacket_i;
    logic [EMPTY_W-1:0] snk_base_empty_i;
    logic               snk_base_ready_o;

    logic [DATA_W-1:0]  snk_to_insert_data_i;
    logic               snk_to_insert_valid_i;
    logic               snk_to_insert_startofpacket_i;
    logic               snk_to_insert_endofpacket_i;
    logic [EMPTY_W-1:0] snk_to_insert_empty_i;
    logic               snk_to_insert_ready_o;

    logic [DATA_W-1:0]  src_data_o;
    logic               src_valid_o;
    logic               src_startofpacket_o;
    logic               src_endofpacket_o;
    logic [EMPTY_W-1:0] src_empty_o;
    logic               src_ready_i; 


    pkt_merge #(.DATA_W(64), .EMPTY_W(3)) DUT(.*);

    int insert_ptr_word_read, base_ptr_word_read, insert_ptr_word_write, base_ptr_word_write;
    assign insert_ptr_word_read = DUT.insert_ptr_word_read;
    assign base_ptr_word_read = DUT.base_ptr_word_read;
    assign insert_ptr_word_write = DUT.insert_ptr_word_write;
    assign base_ptr_word_write = DUT.base_ptr_word_write;

    assign base_left_from_stage = DUT.base_left_from_stage;
    assign insert_left_from_stage = DUT.insert_left_from_stage;

    int base_max_ptr, insert_max_ptr;
    assign base_max_ptr = DUT.base_max_ptr;
    assign insert_max_ptr = DUT.insert_max_ptr;

    logic is_conveyor_stage0_active, is_conveyor_stage1_active, should_conveyor_continue;
    assign is_conveyor_stage0_active = DUT.is_conveyor_stage0_active;
    assign is_conveyor_stage1_active = DUT.is_conveyor_stage1_active;
    assign should_conveyor_continue = DUT.should_conveyor_continue;

    bit is_offset_equal_to_base_packet_length;
    assign is_offset_equal_to_base_packet_length = DUT.is_offset_equal_to_base_packet_length;
    int insert_would_be_at_this_word;
    assign insert_would_be_at_this_word = DUT.insert_would_be_at_this_word;

    bit is_it_one_word_base_pkt, is_it_one_word_insert_pkt;
    assign is_it_one_word_base_pkt = DUT.is_it_one_word_base_pkt;
    assign is_it_one_word_insert_pkt = DUT.is_it_one_word_insert_pkt;

    bit did_conveyor_start_stopping, is_conveyor_stoped;
    assign did_conveyor_start_stopping = DUT.did_conveyor_start_stopping;
    assign is_conveyor_stoped = DUT.is_conveyor_stoped;

    initial begin
        offset_i <= 1;

        src_ready_i <= 1;
        snk_base_startofpacket_i <= 0;
        snk_to_insert_startofpacket_i <= 0;
        snk_base_valid_i <= 0;
        snk_to_insert_valid_i <= 0;
        snk_base_endofpacket_i = 0;
        snk_to_insert_endofpacket_i <= 0;

        //1
        @(posedge clk_i);
        snk_to_insert_data_i <= {8'h1a, 8'h19, 8'h18, 8'h17, 8'h16, 8'h15, 8'h14, 8'h13};
        snk_to_insert_startofpacket_i <= 1;
        snk_to_insert_endofpacket_i <= 1;
        snk_to_insert_valid_i <= 1;
        snk_to_insert_empty_i <= 7;
        
        snk_base_data_i <= {8'h08, 8'h07, 8'h06, 8'h05, 8'h04, 8'h03, 8'h02, 8'h01};
        snk_base_valid_i <= 1;
        snk_base_startofpacket_i <= 1;
        snk_base_endofpacket_i <= 1;
        snk_base_empty_i <= 3;
        

        //2
        @(negedge snk_base_ready_o);
        snk_to_insert_data_i <= {8'h22, 8'h21, 8'h20, 8'h1f, 8'h1e, 8'h1d, 8'h1c, 8'h1b};
        snk_to_insert_empty_i <= 1;
        @(negedge snk_to_insert_ready_o);
        snk_base_data_i <= {8'h10, 8'h0f, 8'h0e, 8'h0d, 8'h0c, 8'h0b, 8'h0a, 8'h09};
        snk_base_empty_i <= 1;


        //3
        @(negedge snk_base_ready_o);
        snk_to_insert_data_i <= {8'h2a, 8'h29, 8'h28, 8'h27, 8'h26, 8'h25, 8'h24, 8'h23};
        snk_to_insert_empty_i <= 0;
        @(negedge snk_to_insert_ready_o);
        snk_base_data_i <= {8'h18, 8'h17, 8'h16, 8'h15, 8'h14, 8'h13, 8'h12, 8'h11};
        snk_base_empty_i <= 0;

    end


    initial begin
        clk_i = 0;
        forever #5 clk_i = ~clk_i;
    end

endmodule