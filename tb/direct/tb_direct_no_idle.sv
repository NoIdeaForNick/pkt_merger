timeunit 1ns;
timeprecision 1ns;

module tb_shift_merge;

    localparam DATA_W    = 64;
    localparam DATA_BITS_PER_SYMBOL = 8;          
    localparam EMPTY_W   = $clog2(DATA_W/DATA_BITS_PER_SYMBOL);
    
    logic               clk_i;
    logic               rst_i;
    logic [15:0]        offset_i;
    logic [DATA_W-1:0]  snk_base_data_i;
    logic               snk_base_valid_i;
    logic               snk_base_startofpacket_i;
    logic               snk_base_endofpacket_i;
    logic [EMPTY_W-1:0] snk_base_empty_i;
    logic               snk_base_ready_o;

    logic [DATA_W-1:0]  snk_to_insert_data_i;
    logic               snk_to_insert_valid_i;
    logic               snk_to_insert_startofpacket_i;
    logic               snk_to_insert_endofpacket_i;
    logic [EMPTY_W-1:0] snk_to_insert_empty_i;
    logic               snk_to_insert_ready_o;

    logic [DATA_W-1:0]  src_data_o;
    logic               src_valid_o;
    logic               src_startofpacket_o;
    logic               src_endofpacket_o;
    logic [EMPTY_W-1:0] src_empty_o;
    logic               src_ready_i; 


    pkt_merge #(.DATA_W(64), .EMPTY_W(3)) DUT(.*);

    int insert_ptr_word_read, base_ptr_word_read, insert_ptr_word_write, base_ptr_word_write;
    assign insert_ptr_word_read = DUT.insert_ptr_word_read;
    assign base_ptr_word_read = DUT.base_ptr_word_read;
    assign insert_ptr_word_write = DUT.insert_ptr_word_write;
    assign base_ptr_word_write = DUT.base_ptr_word_write;

    assign base_left_from_stage = DUT.base_left_from_stage;
    assign insert_left_from_stage = DUT.insert_left_from_stage;

    int base_max_ptr, insert_max_ptr;
    assign base_max_ptr = DUT.base_max_ptr;
    assign insert_max_ptr = DUT.insert_max_ptr;

    logic is_conveyor_stage0_active, is_conveyor_stage1_active, should_conveyor_continue;
    assign is_conveyor_stage0_active = DUT.is_conveyor_stage0_active;
    assign is_conveyor_stage1_active = DUT.is_conveyor_stage1_active;
    assign should_conveyor_continue = DUT.should_conveyor_continue;

    bit is_offset_equal_to_base_packet_length;
    assign is_offset_equal_to_base_packet_length = DUT.is_offset_equal_to_base_packet_length;
    int insert_would_be_at_this_word;
    assign insert_would_be_at_this_word = DUT.insert_would_be_at_this_word;

    bit is_it_one_word_base_pkt, is_it_one_word_insert_pkt;
    assign is_it_one_word_base_pkt = DUT.is_it_one_word_base_pkt;
    assign is_it_one_word_insert_pkt = DUT.is_it_one_word_insert_pkt;

    bit did_conveyor_start_stopping, is_conveyor_stoped;
    assign did_conveyor_start_stopping = DUT.did_conveyor_start_stopping;
    assign is_conveyor_stoped = DUT.is_conveyor_stoped;



    event test_one_word_base, test_one_word_insert, one_word_finished_base, one_word_finished_insert;
    event test_many_words_base, test_many_words_insert;
    int number_of_itterations = 8;

    initial begin
        rst_i <= 0;
        offset_i <= 9;
        
        snk_base_valid_i <= 0;
        snk_base_startofpacket_i <= 0;
        snk_base_endofpacket_i = 0;

        snk_to_insert_valid_i <= 0;
        snk_to_insert_startofpacket_i <= 0;
        snk_to_insert_endofpacket_i <= 0;

        @(posedge clk_i);
 //       -> test_one_word_base;
 //       -> test_one_word_insert;
 //       wait(one_word_finished_base.triggered);
 //       wait(one_word_finished_insert.triggered);

        -> test_many_words_base;
        -> test_many_words_insert;
    end

    //one word
    always @(test_one_word_base)
    begin
        
        snk_base_valid_i <= 1;
        snk_base_startofpacket_i <= 1;
        snk_base_endofpacket_i = 1;

        for(int i=0; i<number_of_itterations; ++i)
        begin
            bit [63:0] word;
            automatic byte unsigned databyte_in_word = i;
            repeat(8)
            begin
                word = {databyte_in_word, word[63:$bits(databyte_in_word)]};
                ++databyte_in_word;
            end
            snk_base_data_i <= word;
            snk_base_empty_i <= i;
            
            @(negedge snk_base_ready_o);
        end
    end


    always @(test_one_word_insert)
    begin
        snk_to_insert_valid_i <= 1;
        snk_to_insert_startofpacket_i <= 1;
        snk_to_insert_endofpacket_i <= 1;

        for(int i=0; i<number_of_itterations; ++i)
        begin
            bit [63:0] word;
            automatic byte unsigned databyte_in_word = i + 8'h13;

            repeat(8)
            begin
                word = {databyte_in_word, word[63:$bits(databyte_in_word)]};
                ++databyte_in_word;
            end
            snk_to_insert_empty_i <= i;
            snk_to_insert_data_i <= word;
            
            @(negedge snk_to_insert_ready_o);
        end
    end

    //many words
    always @(test_many_words_base)
    begin
        
        snk_base_valid_i <= 1;

        for(int i=0; i<number_of_itterations; ++i)
        begin
            for(int j=0; j<4; ++j)
            begin
                bit [63:0] word;
                automatic byte unsigned databyte_in_word = j*8;
                snk_base_empty_i <= i;
                repeat(8)
                begin
                    word = {databyte_in_word, word[63:$bits(databyte_in_word)]};
                    ++databyte_in_word;
                end
                snk_base_data_i <= word;

                if(j == 0) snk_base_startofpacket_i <= 1;

                if(j == 1) snk_base_startofpacket_i <= 0;
                    
                if(j == 2) snk_base_endofpacket_i <= 1;

                if(j == 3)
                begin
                    snk_base_startofpacket_i <= 1; 
                    snk_base_endofpacket_i <= 0;
                end    
                @(posedge clk_i);
            end
            @(posedge snk_base_ready_o);
        end

        snk_base_valid_i <= 0;
    end

    always @(test_many_words_insert)
    begin

        snk_to_insert_valid_i <= 1;

        for(int i=0; i<number_of_itterations; ++i)
        begin
            for(int j=0; j<15; ++j)
            begin
                bit [63:0] word;
                automatic byte unsigned databyte_in_word = j*8 + 8'h13;
                snk_to_insert_empty_i <= i;
                repeat(8)
                begin
                    word = {databyte_in_word, word[63:$bits(databyte_in_word)]};
                    ++databyte_in_word;
                end
                snk_to_insert_data_i <= word;

                if(j == 0) snk_to_insert_startofpacket_i <= 1;

                if(j == 1) snk_to_insert_startofpacket_i <= 0;

                if(j == 2) snk_to_insert_valid_i <= 0;

                if(j == 12) snk_to_insert_valid_i <= 1;
                   
                   
                if(j == 13) snk_to_insert_endofpacket_i <= 1;


                if(j == 14)
                begin 
                    snk_to_insert_startofpacket_i <= 1;
                    snk_to_insert_endofpacket_i <= 0;
                end    
                @(posedge clk_i);
            end
            
            @(posedge snk_to_insert_ready_o);
        end

        snk_to_insert_valid_i <= 0;
    end


    initial begin
        src_ready_i = 1;
        forever
        begin
            automatic shortint unsigned next_device_busy_delay_before_pkt = $urandom_range(100, 0);
            automatic shortint unsigned next_device_busy_delay_through_pkt1 = $urandom_range(50, 0);
            automatic shortint unsigned next_device_busy_delay_through_pkt2 = $urandom_range(100, 0);

            //inside merge
            @(posedge src_valid_o);
            #(next_device_busy_delay_through_pkt1 * 1ns);
            @(posedge clk_i); //align to clk
            src_ready_i = 0;
            #(next_device_busy_delay_through_pkt2 * 1ns);
            @(posedge clk_i); //align to clk
            src_ready_i = 1;

            //before merge
            @(negedge src_endofpacket_o);
            #(next_device_busy_delay_before_pkt * 1ns);
            @(posedge clk_i); //align to clk
            src_ready_i = 0;
            #(next_device_busy_delay_before_pkt * 1ns);
            @(posedge clk_i); //align to clk
            src_ready_i = 1;
        end
    end



    initial begin
        clk_i = 0;
        forever #5 clk_i = ~clk_i;
    end

endmodule