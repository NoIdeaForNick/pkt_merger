`ifndef SEQUENCER
`define SEQUENCER

class sequencer;
    transaction base_trans, insert_trans;

    mailbox base_seq2driv, insert_seq2driv;    
    static int unsigned qty_of_packets_to_be_sended = 1;
    static shortint unsigned delay_between_iterations = 0;
    event finished;

    function new(mailbox base_seq2driv, insert_seq2driv);
        this.base_seq2driv = base_seq2driv;
        this.insert_seq2driv = insert_seq2driv;
    endfunction

    static function void SetNumberOfIterations(int unsigned iterations);
        qty_of_packets_to_be_sended = iterations;
    endfunction

    static function void SetDelayBetweenPackets(shortint unsigned delay);
        delay_between_iterations = delay;
    endfunction

    virtual function transaction GetBaseItem();
        transaction base_trans;        
        base_trans = new();
        assert(base_trans.randomize()) else ErrorHandler("Full cover", "Base packet");
        return(base_trans);
    endfunction

    virtual function transaction GetInsertItem();
        transaction insert_trans;        
        insert_trans = new();
        assert(insert_trans.randomize()) else ErrorHandler("Full cover", "Insert packet");
        return(insert_trans);
    endfunction

    function void ErrorHandler(string object_failed, pkt_type);
        $error("%m %t %s %s randomization failed!", $time, pkt_type, object_failed);
        $stop;
    endfunction

    task IsDelayExpired(shortint unsigned delay);
        while(delay)
        begin
           #1; 
           --delay;
        end
    endtask

    task main();
        shortint unsigned base_pkt_delay, insert_pkt_delay;        

        repeat(qty_of_packets_to_be_sended)
        begin
            //creating objects
            fork
                base_trans = GetBaseItem();
                insert_trans = GetInsertItem();
            join

            //setting sending delays
            base_pkt_delay = base_trans.delay;
            insert_pkt_delay = insert_trans.delay;

            //sending created objects
            fork
                begin
                    IsDelayExpired(base_pkt_delay);
                    base_seq2driv.put(base_trans);
                end

                begin
                    IsDelayExpired(insert_pkt_delay);
                    insert_seq2driv.put(insert_trans);
                end
            join
            IsDelayExpired(delay_between_iterations);
        end
        -> finished;
    endtask
endclass


class special_cases_sequencer extends sequencer;
    string transaction_type;

    function new(mailbox base_seq2driv, insert_seq2driv, string transaction_type);
        super.new(base_seq2driv, insert_seq2driv);
        this.transaction_type = transaction_type;
    endfunction

    virtual function transaction GetBaseItem();
        transaction base_trans;
        small_base_pkt small_base_trans;
        full_offset_small_base_pkt full_offset_small_base_trans;
        one_word_base_pkt one_word_base_trans;
        no_empty_small_base_pkt no_empty_small_base_trans;
        full_empty_small_base_pkt full_empty_small_base_trans;
        full_house_base_pkt full_house_base_trans;
        no_delay_small_base_pkt no_delay_small_base_trans;

        case(transaction_type)

            "small_pkt":
            begin               
                small_base_trans = new();
                assert(small_base_trans.randomize()) else ErrorHandler(transaction_type, "Base");
                base_trans = small_base_trans;
            end

            "full_offset_pkt":
            begin
                full_offset_small_base_trans = new();
                assert(full_offset_small_base_trans.randomize()) else ErrorHandler(transaction_type, "Base");
                base_trans = full_offset_small_base_trans;
            end

            "one_word_pkt":
            begin
                one_word_base_trans = new();
                assert(one_word_base_trans.randomize()) else ErrorHandler(transaction_type, "Base");
                base_trans = one_word_base_trans;
            end

            "no_empty_small_pkt":
            begin
                no_empty_small_base_trans = new();
                assert(no_empty_small_base_trans.randomize()) else ErrorHandler(transaction_type, "Base");
                base_trans = no_empty_small_base_trans;
            end

            "full_empty_small_pkt":
            begin
                full_empty_small_base_trans = new();
                assert(full_empty_small_base_trans.randomize()) else ErrorHandler(transaction_type, "Base");
                base_trans = full_empty_small_base_trans;
            end

            "full_house_pkt":
            begin
                full_house_base_trans = new();
                assert(full_house_base_trans.randomize()) else ErrorHandler(transaction_type, "Base");
                base_trans = full_house_base_trans;
            end

            "no_delay_small_pkt":
            begin
                no_delay_small_base_trans = new();
                assert(no_delay_small_base_trans.randomize()) else ErrorHandler(transaction_type, "Base");
                base_trans = no_delay_small_base_trans;
            end

            default:
            begin
                small_base_trans = new();
                assert(small_base_trans.randomize()) else ErrorHandler(transaction_type, "Base");
                base_trans = small_base_trans; 
            end

        endcase             
        return(base_trans);
    endfunction

    virtual function transaction GetInsertItem();
        transaction insert_trans;
        small_insert_pkt small_insert_trans;
        one_word_insert_pkt one_word_insert_trans;
        no_empty_small_insert_pkt no_empty_small_insert_trans;
        full_empty_small_insert_pkt full_empty_small_insert_trans;
        full_house_insert_pkt full_house_insert_trans;
        no_delay_small_insert_pkt no_delay_small_insert_trans;

        case(transaction_type)

            "small_pkt":
            begin
                small_insert_trans = new();
                assert(small_insert_trans.randomize()) else ErrorHandler(transaction_type, "Insert");
                insert_trans = small_insert_trans;
            end

            "one_word_pkt":
            begin
                one_word_insert_trans = new();
                assert(one_word_insert_trans.randomize()) else ErrorHandler(transaction_type, "Insert");
                insert_trans = one_word_insert_trans;
            end

            "no_empty_small_pkt":
            begin
                no_empty_small_insert_trans = new();
                assert(no_empty_small_insert_trans.randomize()) else ErrorHandler(transaction_type, "Insert");
                insert_trans = no_empty_small_insert_trans;
            end

            "full_empty_small_pkt":
            begin
                full_empty_small_insert_trans = new();
                assert(full_empty_small_insert_trans.randomize()) else ErrorHandler(transaction_type, "Insert");
                insert_trans = full_empty_small_insert_trans;
            end

            "full_house_pkt":
            begin
                full_house_insert_trans = new();
                assert(full_house_insert_trans.randomize()) else ErrorHandler(transaction_type, "Insert");
                insert_trans = full_house_insert_trans;
            end

            "no_delay_small_pkt":
            begin
                no_delay_small_insert_trans = new();
                assert(no_delay_small_insert_trans.randomize()) else ErrorHandler(transaction_type, "Insert");
                insert_trans = no_delay_small_insert_trans;
            end

            default:
            begin
               small_insert_trans = new();
               assert(small_insert_trans.randomize()) else ErrorHandler(transaction_type, "Insert");
               insert_trans = small_insert_trans;
            end

        endcase 
        return(insert_trans);
    endfunction
endclass

`endif