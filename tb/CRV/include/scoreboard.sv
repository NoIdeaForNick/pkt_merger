`ifndef SCOREBOARD
`define SCOREBOARD

class scoreboard;
    string transaction_type;
    int unsigned number_of_merged_packets;
    mailbox mon2scb;
    bit is_transcripts_on;

    function new(mailbox mon2scb, string transaction_type, bit is_transcripts_on = 1);
        this.mon2scb = mon2scb;
        this.transaction_type = transaction_type;
        this.is_transcripts_on = is_transcripts_on;
    endfunction

    function void ShowPacket(string pkt_name, transaction trans);
        $displayh("%m %t %s body\t = %p\t empty = %d", $time, pkt_name, trans.pkt_body, trans.empty_bytes_in_last_word);
    endfunction

    function void ShowAllMergingData(transaction base_pkt, insert_pkt, merged_pkt);
        $display("%m %t offset = %d", $time, base_pkt.offset);
        ShowPacket("base", base_pkt);
        ShowPacket("inset", insert_pkt);
        ShowPacket("merged", merged_pkt);        
    endfunction

    function void PlaceQueueIntoDynamicArray(transaction trans, ref byte unsigned arr[]);
        int unsigned pkt_size = trans.pkt_body.size();
        for(int unsigned i=0; i<pkt_size; ++i) //goes via all packet
        begin           
            bit [trans.WORD_WIDTH_BITS-1:0] buffer_word = trans.pkt_body.pop_front();
            for(int j=0; j<trans.BYTES_IN_ONE_WORD; ++j) //goes via one word
            begin
                arr[i*trans.BYTES_IN_ONE_WORD+j] = buffer_word[j*8+:8];
            end
        end
    endfunction

    function bit CheckMergedPkt(transaction base_pkt, insert_pkt, merged_pkt, bit is_transcripts_on);    
        byte unsigned base_arr[], insert_arr[], merged_arr[], expected_merged_arr[];
        base_arr = new[base_pkt.pkt_body.size()*base_pkt.BYTES_IN_ONE_WORD - base_pkt.empty_bytes_in_last_word];       
        insert_arr = new[insert_pkt.pkt_body.size()*insert_pkt.BYTES_IN_ONE_WORD - insert_pkt.empty_bytes_in_last_word];
        merged_arr = new[merged_pkt.pkt_body.size()*merged_pkt.BYTES_IN_ONE_WORD - merged_pkt.empty_bytes_in_last_word]; //from module
        expected_merged_arr = new[base_arr.size() + insert_arr.size()]; //from function

        if(is_transcripts_on)
        begin    
            $display("base arr size   = %d", base_arr.size());
            $display("insert arr size = %d", insert_arr.size());
            $display("merged arr size = %d", merged_arr.size());
        end

        PlaceQueueIntoDynamicArray(base_pkt, base_arr);
        PlaceQueueIntoDynamicArray(insert_pkt, insert_arr);
        PlaceQueueIntoDynamicArray(merged_pkt, merged_arr);

        for(int unsigned i=0; i<expected_merged_arr.size(); ++i)
        begin
            if(i < base_pkt.offset) expected_merged_arr[i] = base_arr[i];    
            else
            begin
                if(i < (base_pkt.offset + insert_arr.size())) expected_merged_arr[i] = insert_arr[i-base_pkt.offset];
                else expected_merged_arr[i] = base_arr[i-insert_arr.size()];
            end   
        end

        if(is_transcripts_on)
        begin
            $displayh("base arr            = %p", base_arr);
            $displayh("insert arr          = %p", insert_arr);
            $displayh("merged arr          = %p", merged_arr);
            $displayh("expexted merged arr = %p", expected_merged_arr);
        end

        return(merged_arr == expected_merged_arr);
    endfunction

    task main();
        transaction base_pkt, insert_pkt, merged_pkt;
        forever begin
            mon2scb.get(base_pkt);
            mon2scb.get(insert_pkt);
            mon2scb.get(merged_pkt);
            if(is_transcripts_on) ShowAllMergingData(base_pkt, insert_pkt, merged_pkt);                 
            assert(CheckMergedPkt(base_pkt, insert_pkt, merged_pkt, is_transcripts_on)) 
            begin
                if(is_transcripts_on)
                begin
                    $display("------------------------");
                    $display("\tYay! Correct!"); 
                    $display("number of iterations = %0d of testing %s", number_of_merged_packets + 1, transaction_type);
                    $display("------------------------");
                end
            end    
            else 
            begin
                $error("MERGING WENT WRONG!!! =("); 
                $error("failed on transaction type = %s", transaction_type); 
                void'(CheckMergedPkt(base_pkt, insert_pkt, merged_pkt, 1)); //just show wrong stuff with transcripts  
                #100;         
                $stop;
            end    
            ++number_of_merged_packets;
        end
    endtask
endclass

`endif