`ifndef ENVIRONMEMT
`define ENVIRONMEMT

class environment;
    string transaction_type;

    sequencer seq;
    driver driv;
    monitor mon;
    scoreboard scb;

    mailbox base_seq2driv, insert_seq2driv, mon2scb;
    virtual base_if base_vif;
    virtual insert_if insert_vif;
    virtual merged_if merged_vif;
    bit is_transcripts_on;

    function new(virtual base_if base_vif, virtual insert_if insert_vif, virtual merged_if merged_vif, 
                    string transaction_type = "not set", bit is_transcripts_on = 1);            
        this.base_vif = base_vif;
        this.insert_vif = insert_vif;
        this.merged_vif = merged_vif;
        this.transaction_type = transaction_type;  
        this.is_transcripts_on = is_transcripts_on;    
    endfunction


    function void CreateObjects();
        base_seq2driv = new();
        insert_seq2driv = new();
        mon2scb = new();
        seq = GetSeqItem(base_seq2driv, insert_seq2driv);
        driv = new(base_vif, insert_vif, base_seq2driv, insert_seq2driv);
        mon = new(base_vif, insert_vif, merged_vif, mon2scb);
        scb = new(mon2scb, transaction_type, is_transcripts_on);  
    endfunction


    virtual function sequencer GetSeqItem(mailbox base_seq2driv, insert_seq2driv);        
        sequencer seq;
        seq = new(base_seq2driv, insert_seq2driv);
        return(seq);
    endfunction


    task HaltAndReset();
        disable Run;        
        Run();
    endtask


    task PreTest();        
        driv.Reset();
    endtask


    task Test();
        disable driv.main;
        disable mon.main;
        disable scb.main;
        fork
            seq.main();
            driv.main();
            mon.main();
            scb.main();
        join_any
    endtask


    task PostTest();
        wait(seq.finished.triggered);
        wait(seq.qty_of_packets_to_be_sended == driv.number_of_transaction);
        wait(driv.number_of_transaction == scb.number_of_merged_packets);
    endtask


    task Run();
        CreateObjects();
        PreTest();
        Test();
        PostTest();
    endtask

endclass


class special_cases_environment extends environment;  
    function new(virtual base_if base_vif, virtual insert_if insert_vif, virtual merged_if merged_vif, string transaction_type = "not set");
        super.new(base_vif, insert_vif, merged_vif, transaction_type);
    endfunction

    virtual function sequencer GetSeqItem(mailbox base_seq2driv, insert_seq2driv);        
        special_cases_sequencer sc_seq;
        sc_seq = new(base_seq2driv, insert_seq2driv, transaction_type);
        return(sc_seq);
    endfunction
endclass

`endif