`ifndef MONITOR
`define MONITOR

class monitor;
    virtual base_if base_vif;
    virtual insert_if insert_vif;
    virtual merged_if merged_vif;
    mailbox mon2scb;

    function new(virtual base_if base_vif, virtual insert_if insert_vif, virtual merged_if merged_vif, mailbox mon2scb);
        this.base_vif = base_vif;
        this.insert_vif = insert_vif;
        this.merged_vif = merged_vif;
        this.mon2scb = mon2scb;
    endfunction

    task main();
        forever begin
            transaction base_pkt, insert_pkt, merged_pkt;
            base_pkt = new();
            insert_pkt = new();
            merged_pkt = new();
            
            //parsing packets
            fork
                //base
                begin
                     
                    wait(base_vif.snk_base_valid_i && base_vif.snk_base_startofpacket_i && base_vif.snk_base_ready_o); //waiting till packet comes
                    @(posedge base_vif.clk);
                    forever
                    begin
                        if(base_vif.snk_base_valid_i) //module does not put ready low while getting paket. and race condition happends - diffrent regions
                        begin 
                            base_pkt.pkt_body.push_back(base_vif.snk_base_data_i);
                            ++base_pkt.pkt_length_in_words;
                            if(base_vif.snk_base_endofpacket_i) 
                            begin
                                base_pkt.offset = base_vif.offset_i;
                                base_pkt.empty_bytes_in_last_word = base_vif.snk_base_empty_i; 
                                break;    
                            end    
                        end 
                        @(posedge base_vif.clk);
                    end  
                end

                //insert
                begin

                    wait(insert_vif.snk_to_insert_valid_i && insert_vif.snk_to_insert_startofpacket_i && insert_vif.snk_to_insert_ready_o);
                    @(posedge insert_vif.clk);
                    forever
                    begin
                        if(insert_vif.snk_to_insert_valid_i)
                        begin
                            insert_pkt.pkt_body.push_back(insert_vif.snk_to_insert_data_i);
                            ++insert_pkt.pkt_length_in_words;
                            if(insert_vif.snk_to_insert_endofpacket_i)
                            begin 
                                insert_pkt.empty_bytes_in_last_word = insert_vif.snk_to_insert_empty_i;
                                break;
                            end    
                        end    
                        @(posedge insert_vif.clk);
                    end                
                end

                //merged
                begin
                    do @(posedge merged_vif.clk);
                    while(!(merged_vif.src_valid_o && merged_vif.src_startofpacket_o && merged_vif.src_ready_i));

                    forever
                    begin
                        if(merged_vif.src_valid_o && merged_vif.src_ready_i)
                        begin
                            merged_pkt.pkt_body.push_back(merged_vif.src_data_o);
                            ++merged_pkt.pkt_length_in_words;
                            if(merged_vif.src_endofpacket_o)
                            begin 
                                merged_pkt.empty_bytes_in_last_word = merged_vif.src_empty_o;
                                @(posedge merged_vif.clk);
                                if(merged_vif.src_endofpacket_o && merged_vif.src_ready_i) $error("Stuck at last word...");
                                break;
                            end    
                        end 
                        @(posedge merged_vif.clk); 
                    end   
                end
            join

            //sending objects via mail
            mon2scb.put(base_pkt);
            mon2scb.put(insert_pkt);
            mon2scb.put(merged_pkt);
        end
    endtask
endclass

`endif