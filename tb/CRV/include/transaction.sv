`ifndef TRANSACTION
`define TRANSACTION

class transaction;
    localparam BYTES_IN_ONE_WORD = 8;
    localparam WORD_WIDTH_BITS = 64;
    localparam MAX_BYTES_IN_PACKET = 65_535;
    localparam MAX_WORDS_IN_PACKET = 8192;    

    rand shortint unsigned pkt_length_in_words; //1-8192
    rand bit [2:0] empty_bytes_in_last_word;
    rand shortint unsigned delay;  //starting delay
    rand bit [15:0] offset;
    
    rand bit is_low_valid_allowed_in_pkt; //valid might go low during transmission
    rand byte unsigned word_number_when_valid_goes_low; //when it goes low
    rand byte unsigned valid_goes_low_for_number_of_tiks;

    
    //order pkt_length_in_words |-> empty_bytes_in_last_word |-> offset
    //vars
    constraint c_pkt_length {pkt_length_in_words inside {[1:MAX_WORDS_IN_PACKET]};
                             solve pkt_length_in_words before empty_bytes_in_last_word;}

    constraint c_delay {delay inside {[1:3000]};}

    constraint c_empty_bytes {empty_bytes_in_last_word inside {[0:7]}; 
                            if(pkt_length_in_words == MAX_WORDS_IN_PACKET) empty_bytes_in_last_word >= 1;
                            else empty_bytes_in_last_word >= 0;
                            solve empty_bytes_in_last_word before offset;} //if we have 8192 words so empty should be greater or equal to 1: bytes <= 65535
    
    constraint c_offset {1 <= offset; 
                         offset <= pkt_length_in_words*BYTES_IN_ONE_WORD - empty_bytes_in_last_word;}

    constraint c_word_number_when_valid_goes_low {word_number_when_valid_goes_low < pkt_length_in_words;}   

    constraint c_valid_goes_low_for_number_of_tiks {1 < valid_goes_low_for_number_of_tiks;
                                                    valid_goes_low_for_number_of_tiks < 20;}                  

    bit [WORD_WIDTH_BITS-1:0] pkt_body[$];
    
    //function case members can not be overwritten
    virtual function byte unsigned GetInitialDataByteValue();
        byte unsigned databyte_in_word = 8'h0;
        return(databyte_in_word);
    endfunction        

    //behaves like virtual
    function void post_randomize();
        byte unsigned databyte_in_word = GetInitialDataByteValue();
        for(shortint unsigned i=0; i<pkt_length_in_words; ++i)
        begin            
            bit [WORD_WIDTH_BITS-1:0] word;
            repeat(BYTES_IN_ONE_WORD)
            begin
                word = {databyte_in_word, word[WORD_WIDTH_BITS-1:$bits(databyte_in_word)]};
                ++databyte_in_word;
            end
            pkt_body.push_back(word);
        end
    endfunction

    virtual function void ShowPktInfo();
        $display("pkt_length_in_words = %0d", pkt_length_in_words);
        $display("delay = %0d", delay);
        $displayh("packet = %p", pkt_body);
    endfunction
endclass

//******************SMALL PKTS**********************
class small_base_pkt extends transaction;
    constraint c_pkt_length {pkt_length_in_words inside {[1:10]};}
    constraint c_delay {delay inside {[1:10]};}
endclass


class small_insert_pkt extends transaction;    
    constraint c_pkt_length {pkt_length_in_words inside {[1:10]};}
    constraint c_delay {delay inside {[1:100]};}       

    function byte unsigned GetInitialDataByteValue();
        byte unsigned databyte_in_word = 8'h13;
        return(databyte_in_word);
    endfunction  
endclass
//***********************************************


//********INSERT AFTER BASE SMALL PKTS***********
class full_offset_small_base_pkt extends small_base_pkt;
    constraint c_offset {offset == pkt_length_in_words*BYTES_IN_ONE_WORD - empty_bytes_in_last_word;}
endclass
//**********************************************


//**************ONE WORD PKTS*******************
class one_word_base_pkt extends small_base_pkt;
    constraint c_pkt_length {pkt_length_in_words == 1;}
endclass


class one_word_insert_pkt extends small_insert_pkt;
    constraint c_pkt_length {pkt_length_in_words == 1;}
endclass
//**********************************************


//**************NO EMPTY PKTS*******************
class no_empty_small_base_pkt extends small_base_pkt;
    constraint c_empty_bytes {empty_bytes_in_last_word == 0;}
endclass


class no_empty_small_insert_pkt extends small_insert_pkt;
    constraint c_empty_bytes {empty_bytes_in_last_word == 0;}
endclass
//**********************************************


//**************FULL EMPTY PKTS*****************
class full_empty_small_base_pkt extends small_base_pkt;
    constraint c_empty_bytes {empty_bytes_in_last_word == 7;}
endclass


class full_empty_small_insert_pkt extends small_insert_pkt;
    constraint c_empty_bytes {empty_bytes_in_last_word == 7;}
endclass
//**********************************************


//**************FULL HOUSE PKTS*****************
class full_house_base_pkt extends small_base_pkt;
    constraint c_pkt_length {pkt_length_in_words == 8192;}
endclass


class full_house_insert_pkt extends small_insert_pkt;
    constraint c_pkt_length {pkt_length_in_words == 8192;}
endclass
//**********************************************


//*******************NO DELAY*******************
class no_delay_small_base_pkt extends small_base_pkt;
    constraint c_delay {delay == 0;}
endclass


class no_delay_small_insert_pkt extends small_insert_pkt;
    constraint c_delay {delay == 0;}
endclass
//**********************************************



`endif