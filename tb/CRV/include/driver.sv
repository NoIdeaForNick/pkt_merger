`ifndef DRIVER
`define DRIVER

class driver;
    int unsigned number_of_transaction;
    virtual base_if base_vif;
    virtual insert_if insert_vif;
    mailbox base_seq2driv, insert_seq2driv;  

    function new(virtual base_if base_vif, virtual insert_if insert_vif, mailbox base_seq2driv, insert_seq2driv);
        this.base_vif = base_vif;
        this.insert_vif = insert_vif;
        this.base_seq2driv = base_seq2driv;
        this.insert_seq2driv = insert_seq2driv;
    endfunction

    task Reset();
        base_vif.offset_i <= 0;
        base_vif.snk_base_data_i <= 0;
        base_vif.snk_base_valid_i <= 0;
        base_vif.snk_base_startofpacket_i <= 0;
        base_vif.snk_base_endofpacket_i <= 0;
        base_vif.snk_base_empty_i <= 0;
                                                                                                    
        insert_vif.snk_to_insert_data_i <= 0;
        insert_vif.snk_to_insert_valid_i <= 0;
        insert_vif.snk_to_insert_startofpacket_i <= 0;
        insert_vif.snk_to_insert_endofpacket_i <= 0;
        insert_vif.snk_to_insert_empty_i <= 0;
    endtask

    task main();
        forever begin
            transaction base_trans, insert_trans;
            fork
                //base
                begin
                    shortint unsigned original_word_size_of_pkt;
                    byte unsigned word_number_when_valid_goes_low; 
                    byte unsigned valid_goes_low_for_number_of_tiks, valid_low_tiks_counter;
                    bit is_low_valid_allowed_in_pkt;

                    base_seq2driv.get(base_trans);
                    original_word_size_of_pkt = base_trans.pkt_body.size();
                    word_number_when_valid_goes_low = base_trans.word_number_when_valid_goes_low;
                    is_low_valid_allowed_in_pkt = base_trans.is_low_valid_allowed_in_pkt;
                    valid_goes_low_for_number_of_tiks = base_trans.valid_goes_low_for_number_of_tiks;

                    @(posedge base_vif.clk);
                    wait(base_vif.snk_base_ready_o);
                    base_vif.offset_i <= base_trans.offset;
                    
                    @(posedge base_vif.clk); 
                    base_vif.snk_base_empty_i <= base_trans.empty_bytes_in_last_word;
                    base_vif.snk_base_startofpacket_i <= 1;                 
                    if(base_trans.pkt_length_in_words == 1) base_vif.snk_base_endofpacket_i <= 1; //is it one word pkt
                    for(shortint unsigned i=0; i < (is_low_valid_allowed_in_pkt ? original_word_size_of_pkt + valid_goes_low_for_number_of_tiks : original_word_size_of_pkt); ++i)
                    begin
                        if((word_number_when_valid_goes_low == i && is_low_valid_allowed_in_pkt) || valid_low_tiks_counter) //first time enter with first condition, then second
                        begin
                            base_vif.snk_base_valid_i <= 0;
                            @(posedge base_vif.clk);
                            if(valid_low_tiks_counter == valid_goes_low_for_number_of_tiks - 1)
                            begin
                                valid_low_tiks_counter = 0;
                                continue; //go to valid data
                            end
                            else ++valid_low_tiks_counter;
                        end
                        else
                        begin 
                            base_vif.snk_base_valid_i <= 1;
                            base_vif.snk_base_data_i <= base_trans.pkt_body.pop_front();                
                            if(i == (is_low_valid_allowed_in_pkt ? original_word_size_of_pkt + valid_goes_low_for_number_of_tiks - 1 : original_word_size_of_pkt - 1)) base_vif.snk_base_endofpacket_i <= 1; //is it second last word                            
                            @(posedge base_vif.clk);
                            base_vif.snk_base_startofpacket_i <= 0;    
                        end    
                    end
                    base_vif.snk_base_valid_i <= 0;
                    base_vif.snk_base_endofpacket_i <= 0;     
                end

                //insert
                begin
                    shortint unsigned original_word_size_of_pkt;
                    byte unsigned word_number_when_valid_goes_low; 
                    byte unsigned valid_goes_low_for_number_of_tiks, valid_low_tiks_counter;
                    bit is_low_valid_allowed_in_pkt;

                    insert_seq2driv.get(insert_trans);
                    original_word_size_of_pkt = insert_trans.pkt_body.size();
                    word_number_when_valid_goes_low = insert_trans.word_number_when_valid_goes_low;
                    is_low_valid_allowed_in_pkt = insert_trans.is_low_valid_allowed_in_pkt;
                    valid_goes_low_for_number_of_tiks = insert_trans.valid_goes_low_for_number_of_tiks; 

                    @(posedge insert_vif.clk);
                    wait(insert_vif.snk_to_insert_ready_o);
                    
                    @(posedge insert_vif.clk);
                    insert_vif.snk_to_insert_empty_i <= insert_trans.empty_bytes_in_last_word;
                    insert_vif.snk_to_insert_startofpacket_i <= 1;
                    if(insert_trans.pkt_length_in_words == 1) insert_vif.snk_to_insert_endofpacket_i <= 1;
                    for(shortint unsigned i=0; i < (is_low_valid_allowed_in_pkt ? original_word_size_of_pkt + valid_goes_low_for_number_of_tiks : original_word_size_of_pkt); ++i)
                    begin
                        if((word_number_when_valid_goes_low == i && is_low_valid_allowed_in_pkt) || valid_low_tiks_counter) 
                        begin
                            insert_vif.snk_to_insert_valid_i <= 0;
                            @(posedge insert_vif.clk);
                            if(valid_low_tiks_counter == valid_goes_low_for_number_of_tiks - 1)
                            begin
                                valid_low_tiks_counter = 0;
                                continue; //go to valid data
                            end
                            else ++valid_low_tiks_counter;  
                        end
                        else
                        begin
                            insert_vif.snk_to_insert_valid_i <= 1;
                            insert_vif.snk_to_insert_data_i <= insert_trans.pkt_body.pop_front();
                            if(i == (is_low_valid_allowed_in_pkt ? original_word_size_of_pkt + valid_goes_low_for_number_of_tiks - 1 : original_word_size_of_pkt - 1)) insert_vif.snk_to_insert_endofpacket_i <= 1; //is it second last word
                            @(posedge insert_vif.clk);
                            insert_vif.snk_to_insert_startofpacket_i <= 0;
                        end    
                    end
                    insert_vif.snk_to_insert_valid_i <= 0;
                    insert_vif.snk_to_insert_endofpacket_i <= 0;
                end
            join
            ++number_of_transaction;
        end
    endtask
endclass

`endif