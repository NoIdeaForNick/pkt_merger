`define TIME_UNIT 1ns
`define TIME_PRECISION 1ns
timeunit `TIME_UNIT;
timeprecision `TIME_PRECISION;

package objects;
    `include "include/transaction.sv"
    `include "include/sequencer.sv"
    `include "include/driver.sv"
    `include "include/monitor.sv"
    `include "include/scoreboard.sv"
    `include "include/environment.sv"
endpackage


function void ShowCongratulation();
    $display("******************");
    $display("------------------");
    $display("TESTBENCH Passed!!! =)");
    $display("------------------");
    $display("******************");
endfunction


//test with data coverage
program coverage_test(base_if b_if, insert_if i_if, merged_if m_if, output bit reset);
    
    covergroup base_insert_stream_data @(posedge b_if.clk);
        //cover check when module starts to transmit
        //offset
        offset_cover: coverpoint b_if.offset_i iff(m_if.src_startofpacket_o);

        //length
        base_length_cover: coverpoint env.seq.base_trans.pkt_length_in_words iff(m_if.src_startofpacket_o)
        {
            option.at_least = 10;
            bins one_word_pkt = {1};
            bins two_word_pkt = {2};
            bins full_house_pkt = {8192};
            bins almost_full_house = {8191};
            bins all_other_pkts = {[3:8190]};
        } 
        insert_length_cover: coverpoint env.seq.insert_trans.pkt_length_in_words iff(m_if.src_startofpacket_o); 

        //empty
        base_empty_cover: coverpoint env.seq.base_trans.empty_bytes_in_last_word iff(m_if.src_startofpacket_o)
        {
            option.at_least = 10;
        } 
        insert_empty_cover: coverpoint env.seq.insert_trans.empty_bytes_in_last_word iff(m_if.src_startofpacket_o)
        {
            option.at_least = 10;
        }

        //delay
        base_delay_cover: coverpoint env.seq.base_trans.delay iff(m_if.src_startofpacket_o)
        {
            option.auto_bin_max = 10;
        }
        insert_delay_cover: coverpoint env.seq.insert_trans.delay iff(m_if.src_startofpacket_o)
        {
            option.auto_bin_max = 10;
        }

        //cross base length x insert length x offset
        base_length_x_insert_length_x_offset: cross base_length_cover, insert_length_cover, offset_cover;

        //cross offset x base empty x insert empty
        offset_x_base_empty_x_insert_empty: cross offset_cover, base_empty_cover, insert_empty_cover;

    endgroup
    
    
    objects::environment env;   
    base_insert_stream_data full_cover;


    initial begin
        $display("Starting tb with coverage");
        ResetModule();
        SetNextModuleIsAlwaysRdy(); //dont check this stuff at current test. use another one
        CreateObjects();
        while(full_cover.get_coverage < 100) env.Run();                
        ShowCongratulation();
        $stop; 
    end

    task ResetModule();
        reset <= 0;
        reset <= #5 1;
        reset <= #10 0;
    endtask

    task CreateObjects();
        env = new(b_if, i_if, m_if, "full coverage", 0);
        full_cover = new();
    endtask

    function void SetNextModuleIsAlwaysRdy();
        m_if.src_ready_i = 1;
    endfunction
endprogram


//test for some special cases, reset statement and busy statement
program special_cases_test(base_if b_if, insert_if i_if, merged_if m_if, output bit reset);
    objects::special_cases_environment sc_env;
    semaphore reset_sem; 

    string objects_for_testing[] = {"small_pkt", "full_offset_pkt", "no_empty_small_pkt", 
                                    "full_empty_small_pkt", "full_house_pkt", "one_word_pkt",
                                    "no_delay_small_pkt"};

    //main test
    initial begin
        sc_env.seq.SetNumberOfIterations(1_000); //it is static method       
        StartTestingThroughAllObjects();
        reset_sem.get(); //waiting till reset occures
        StartTestingThroughAllObjects(); //and start over
        ShowCongratulation();
        $stop;
    end

    //reset procedure
    initial begin        
        shortint unsigned random_time_delay_before, random_time_delay_after;
        random_time_delay_before = $urandom_range(10_000,5_000);
        random_time_delay_after = $urandom_range(10_000,1);
        reset_sem = new();
        reset = 0;
        #(random_time_delay_before * `TIME_UNIT);
        reset = 1; //reset time
        disable StartTestingThroughAllObjects;
        #(random_time_delay_after * `TIME_UNIT);
        reset = 0;
        reset_sem.put();
    end

    //src_ready_i control
    initial begin
        m_if.src_ready_i = 1;
        forever
        begin
            automatic shortint unsigned next_device_busy_delay_before_pkt = $urandom_range(100, 0);
            automatic shortint unsigned next_device_busy_delay_through_pkt1 = $urandom_range(50, 0);
            automatic shortint unsigned next_device_busy_delay_through_pkt2 = $urandom_range(100, 0);

            //inside merge
            @(posedge m_if.src_valid_o);
            #(next_device_busy_delay_through_pkt1 * `TIME_UNIT);
            @(posedge m_if.clk); //align to clk
            m_if.src_ready_i = 0;
            #(next_device_busy_delay_through_pkt2 * `TIME_UNIT);
            @(posedge m_if.clk); //align to clk
            m_if.src_ready_i = 1;

            //before merge
            @(negedge m_if.src_endofpacket_o);
            #(next_device_busy_delay_before_pkt * `TIME_UNIT);
            @(posedge m_if.clk); //align to clk
            m_if.src_ready_i = 0;
            #(next_device_busy_delay_before_pkt * `TIME_UNIT);
            @(posedge m_if.clk); //align to clk
            m_if.src_ready_i = 1;
        end
    end

    task StartTestingThroughAllObjects();
        foreach(objects_for_testing[i])
        begin
            sc_env = new(b_if, i_if, m_if, objects_for_testing[i]);                
            sc_env.Run(); 
        end
    endtask 
endprogram